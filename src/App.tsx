import * as React from 'react';
import './App.css';
import {FC} from "react";
import UserFormContainer from "./components/userform/UserFormContainer";

const  App: FC = () =>  {
  return (
    <div className="App">
      <UserFormContainer />
    </div>
  );
}

export default App;
