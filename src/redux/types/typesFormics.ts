export type InitialValues = {
    firstName: string
    secondName: string
    thirdName: string
    male: string
    date: string
    nationality: string
    documentNumber: string
    document: string
    tariff: string
    alerts: boolean
    phone? : string
    email? : string
    dateEndDoc?: string
    firstNameDoc?: string
    lastNameDoc?: string
    thirdNameDoc?: string
    internationalPerson?: string
} & { [key: string]: any }

export type initStateForm = {
    sendResponse: string
    success: boolean
    isEditMode: boolean
    passengers: Array<PassengerType>
}

export type PassengerType = {
    id: number
    data: InitialValues,
    isEditMode: boolean
    success: boolean
}