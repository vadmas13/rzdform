

export type PropertiesType<T> = T extends {[key: string]: infer U} ?  U : never;
type MyReturnType<T> =  T extends (...args: any[]) => infer R ? R :never; // as ReturnType

export type dispatchThunk<T> = (...args: any[]) => ActionTypes<T>

export type firebaseHooksType = {
    [key: string]: any
}

export type ActionTypes<T> = MyReturnType<PropertiesType<T>>;



