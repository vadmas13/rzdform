import {InitialValues} from "../types/typesFormics";
import {ActionTypes} from "../types/ActionTypes";
import {Dispatch} from "redux";

export const actionsPassenger = {
    ADD_PASSENGER: (passenger: InitialValues, id: number) => ({ type: 'ADD_PASSENGER', passenger, id} as const),
    EDIT_PASSENGER: (id: number) => ({ type: 'EDIT_PASSENGER', id} as const),
    ADD_NEW_PASSENGER: (id: number) => ({ type: 'ADD_NEW_PASSENGER', id} as const),
    DELETE_PASSENGER: (id: number) => ({ type: 'DELETE_PASSENGER', id} as const),
    CHECK_SUCCESS_EDIT: (id: number) => ({ type: 'CHECK_SUCCESS_EDIT', id} as const),
    REFRESH_ID_PASSENGERS: () => ({ type: 'REFRESH_ID_PASSENGERS'} as const),
    RESPONSE_SEND_FORM: (value: string) => ({ type: 'RESPONSE_SEND_FORM', value} as const),
    CLEAR_RESPONSE: () => ({ type: 'CLEAR_RESPONSE'} as const)
}

export const addPassenger = (passenger: InitialValues, id: number) => (dispatch: Dispatch<ActionTypes<typeof actionsPassenger>>) => {
    dispatch(actionsPassenger.ADD_PASSENGER(passenger, id));
    dispatch(actionsPassenger.CHECK_SUCCESS_EDIT(id));
}

export const editPassenger = (id: number) => (dispatch: Dispatch<ActionTypes<typeof actionsPassenger>>) => {
    dispatch(actionsPassenger.EDIT_PASSENGER(id))
}

export const addNewPassenger = (id: number) => (dispatch: Dispatch<ActionTypes<typeof actionsPassenger>>) => {
    dispatch(actionsPassenger.ADD_NEW_PASSENGER(id))
}

export const deletePassenger = (id: number) => async (dispatch: Dispatch<ActionTypes<typeof actionsPassenger>>) => {
    await dispatch(actionsPassenger.DELETE_PASSENGER(id))
    await dispatch(actionsPassenger.CHECK_SUCCESS_EDIT(id));
    await dispatch(actionsPassenger.REFRESH_ID_PASSENGERS());
}

export const responseSendForm = (value: string) => (dispatch: Dispatch<ActionTypes<typeof actionsPassenger>>) => {
    dispatch(actionsPassenger.RESPONSE_SEND_FORM(value))
}

export const clearResponse = () => (dispatch: Dispatch<ActionTypes<typeof actionsPassenger>>) => {
    dispatch(actionsPassenger.CLEAR_RESPONSE())
}

