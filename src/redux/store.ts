import {combineReducers, createStore, applyMiddleware} from "redux";
import { compose } from 'redux';
import thunkMiddleware from "redux-thunk"
import formReducer from "./formReducer";



let reducers = combineReducers({
    form: formReducer
})

export type AppStateType = ReturnType<typeof reducers>

const store = createStore(reducers, compose(
    applyMiddleware(thunkMiddleware),
))

export default store;