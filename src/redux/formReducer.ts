import {initStateForm} from "./types/typesFormics";
import {ActionTypes} from "./types/ActionTypes";
import {actionsPassenger} from "./actions/actionForm";
import {formFields} from "../config/formFields";


const initState = {
    sendResponse: '',
    isEditMode: true,
    success: false,
    passengers: [
        {
            id: 1,
            data: {
                ...formFields
            },
            isEditMode: true,
            success: false
        }
    ]
}


const formReducer = (state: initStateForm = initState, action: ActionTypes<typeof actionsPassenger>) => {
    switch (action.type) {
        case "ADD_PASSENGER":
            return {
                ...state, passengers: [...state.passengers.map(passenger => {
                    if (passenger.id === action.id) {
                        return {
                            ...passenger,
                            isEditMode: false,
                            success: true,
                            data: {...action.passenger}
                        }
                    } else {
                        return passenger
                    }
                })]
            }
        case "CHECK_SUCCESS_EDIT":
            return {
                ...state,
                isEditMode: state.passengers.some(passenger => {
                    if (passenger.id === action.id) {
                        return false
                    } else {
                        return passenger.isEditMode;
                    }
                }),
                success: state.passengers.every(passenger => {
                    if (passenger.id === action.id) {
                        return true
                    } else {
                        return passenger.success;
                    }
                })
            }
        case "EDIT_PASSENGER":
            return {
                ...state, passengers: [...state.passengers.map(passenger => {
                    if (passenger.id === action.id) {
                        return {
                            ...passenger,
                            isEditMode: true,
                            success: false
                        }
                    } else {
                        return passenger
                    }
                })],
                isEditMode: true,
                success: false
            }
        case "ADD_NEW_PASSENGER":
            return {
                ...state, passengers: [...state.passengers,
                    {
                        ...initState.passengers[0],
                        id: action.id
                    }],
                isEditMode: true,
                success: false
            }
        case "DELETE_PASSENGER":
            return {
                ...state, passengers: [...state.passengers.filter(passenger => passenger.id !== action.id)]
            }
        case "REFRESH_ID_PASSENGERS":
            return {
                ...state, passengers: [...state.passengers.map((passenger, i: number) => {
                    return{
                        ...passenger, id: i + 1
                    }
                })]
            }
        case "RESPONSE_SEND_FORM":
            return {...state, sendResponse: action.value }
        case "CLEAR_RESPONSE":
            return {...state, sendResponse: ''}
        default:
            return state;
    }
}


export default formReducer;