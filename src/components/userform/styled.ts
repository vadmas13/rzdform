import styled  from 'styled-components';

export const Container = styled.div`
   max-width: 1000px;
   padding: 20px;
`

export const H1Title = styled.h1`
   color: ${props => props.theme.MainRed};
   font-size: 30px;
   text-align: left;
   margin-bottom: 0;
`
export const HeaderForm = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`

export const DeletePassenger = styled.div`
  font-size: 15px;
  color: ${props => props.theme.MainRed};
  cursor: pointer;
`

export const FormGroup = styled.div`
   display: flex;
   justify-content: center;
   flex-flow: wrap;
`

export const FormField = styled.div`
   display: flex;
   justify-content: center;
   flex-flow: wrap;
`

export const ErrorMsg = styled.p`
  color: ${(props) => props.theme.MainRed};
  text-align: left;
  margin-top: 5px;
  font-size: 13px;
`

export const TitleField = styled.p`
  font-size: 13px;
  height: 20px;
  overflow: hidden;
  font-weight: 600;
  text-align: left;
  margin-bottom: 8px;
  display: flex;
  color: ${(props) => props.theme.TitleFormColor};
 
`

export const TitleBorderText = styled.span<{ border?: string | null }>`
  cursor: pointer;
  border-bottom: 1px dotted #444444;
`

export const TextField = styled.p`
  font-size: 15px;
  color: #444444;
  text-align: left;
`

export const StarUsualField = styled.span`
  font-size: 14px;
  margin-left: 8px;
  color: ${(props) => props.theme.MainRed}
`

