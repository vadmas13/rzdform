import * as React from 'react'
import {FC} from "react";
import {ResponseContainer, ResponseText, IconResponse} from "./styledResponse";
import {Button, Icon} from "semantic-ui-react";

const ResponseFromServer: FC<{response : string, clearResponse?: () => void}> = ({response,clearResponse}) => {

    const goBack = () => {
        if(clearResponse){
            clearResponse()
        }
    }

    return(
        <ResponseContainer>
            <IconResponse>
                <Icon name='js'/>
            </IconResponse>
            <ResponseText>
                { response }
            </ResponseText>
            <Button content="Вернуться"
                    type="submit"
                    onClick={goBack}
                    inverted color='red'/>
        </ResponseContainer>
    )
}

export default ResponseFromServer