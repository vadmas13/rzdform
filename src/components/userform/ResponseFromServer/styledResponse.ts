import styled  from 'styled-components';

export const ResponseContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 100px 0;
    flex-flow: column;
`

export const ResponseText = styled.div`
   font-size: 30px;
   margin-bottom: 30px;
   color: ${props => props.theme.TitleFormColor}
`
export const IconResponse = styled.div`
   font-size: 150px;
   color: ${props => props.theme.TitleFormColor}
`