import * as React from 'react'
import {useField, useFormikContext} from "formik";
import DatePicker, { registerLocale } from "react-datepicker";
import './datePickerStyled.css'
import ru from 'date-fns/locale/ru';
registerLocale('ru', ru)

export const DatePickerField = ({...props}: any) => {
    const {setFieldValue, setFieldTouched} = useFormikContext();
    const [field] = useField(props);
    return (
        <DatePicker
            {...field}
            {...props}
            selected={(field.value && new Date(field.value)) || null}
            locale="ru"
            onChange={(val: any) => {
                setFieldTouched(field.name, true)
                setFieldValue(field.name, val);
            }}
            placeholderText={'__ __ ____'}
        />
    );
};

export default DatePickerField;