import styled from 'styled-components';

export const RowUsers = styled.div`
  display: flex;
  flex-flow: wrap;
  margin-bottom: 30px;
`

export const UserData = styled.div`
  width: 30%;
  flex: 1 1 auto;
  margin-bottom: 10px;
  padding: 10px;
`

