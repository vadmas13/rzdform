import * as React from 'react'
import {FC} from "react";
import {Container, H1Title, TextField} from "../styled";
import { PassengerType } from "../../../redux/types/typesFormics";
import {RowUsers, UserData} from "./styledUserData";
import {formTitles, revertValues} from "../../../config/formFields";
import TitleFieldComp from "../TitleField/TitleFieldComp";
import {Button} from "semantic-ui-react";

type propTypes = {
    dataPassenger: PassengerType
    editPassenger?: (id: number) => void
}

const UserDataSuccess: FC<propTypes> = ({dataPassenger,editPassenger}) => {

    const dataTitles = dataPassenger.data;
    const dataFields: {[key: string]: string} = revertValues(dataTitles);

    const editPassengerMode = () => {
        if(editPassenger){
            editPassenger(dataPassenger.id)
        }
    }

    return (
        <Container>
            <H1Title>
                Пассажир №{dataPassenger.id}
            </H1Title>
            <RowUsers>
                { Object.keys(dataTitles).map(item => {
                    if(dataTitles[item]){
                        return(
                            <UserData>
                                <TitleFieldComp name={formTitles[item]} requiredOff={true}/>
                                <TextField>
                                    { dataFields[item] }
                                </TextField>
                            </UserData>
                        )
                    }else return false
                }) }

            </RowUsers>
            <Button content="Редактировать"
                    style={{float: 'left'}}
                    onClick={() => editPassengerMode()}
                    inverted color='red'/>
        </Container>
    )
}

export default UserDataSuccess