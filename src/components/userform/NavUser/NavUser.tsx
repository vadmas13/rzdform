import * as React from 'react'
import {FC} from 'react'
import styled from './../styledSemantic.module.scss'
import {Button, Icon} from 'semantic-ui-react'
import {initStateForm} from "../../../redux/types/typesFormics";
import {revertValues} from "../../../config/formFields";
import {sendDataToServer} from "../../../config/fetchingData";

type propsType = {
    activePassenger: number
    setActivePassenger: any
    form: initStateForm
    addNewPassenger?: (id: number) => void
    responseSendForm?: (value: string) => void
}

const NavUser: FC<propsType> = ({
                                    activePassenger, setActivePassenger, addNewPassenger, responseSendForm,
                                    form: {passengers, isEditMode, success}
                                }) => {

    const addNewPass = (id: number) => {
        if (addNewPassenger) {
            addNewPassenger(id);
            setActivePassenger(id);
        }
    }


    const sendData = () => {
        const sendData: Array<{ [key: string]: string }> = passengers.map(p => ({...revertValues(p.data)}));
        if (responseSendForm) {
            sendDataToServer(sendData, responseSendForm);
        }
    }

    return (
        <div className={styled.navUser}>
            <Icon name='user circle outline' className={styled.navUser__icon}/>
            <p className={styled.navUser__title}>Пассажир №{activePassenger}</p>
            <div className={styled.usersList}>
                {Object.values(passengers).map((passenger, i: number) => {
                    return (
                        <div className={passenger.isEditMode ? styled.usersList__item :
                            `${styled.usersList__item_success} ${styled.usersList__item}`}
                             onClick={() => setActivePassenger(passenger.id)} key={`${i}_NavUser`}>
                            <p className={styled.usersList__title}>
                                Пассажир №{passenger.id}
                            </p>

                            {
                                passenger.id === activePassenger && passenger.isEditMode ? (
                                    <p className={styled.usersList__textActive}>
                                        В режиме редактирования
                                    </p>
                                ) : (
                                    <p className={styled.usersList__text}>

                                        {passenger.data.firstName ? `${passenger.data.firstName} ${passenger.data.secondName[0]}.${passenger.data.thirdName[0]}.`
                                            : 'Ожидает заполнения'}
                                    </p>
                                )
                            }
                        </div>
                    )
                })}
            </div>
            <div className={styled.navUser__buttons}>
                <Button content="Новый пассажир"
                        onClick={() => addNewPass(passengers.length + 1)}
                        disabled={isEditMode}
                        style={{marginBottom: '10px'}}
                        inverted color='red'/>
                <Button content="Перейти к оплате"
                        onClick={() => sendData()}
                        disabled={!success}
                        inverted color='red'/>
            </div>
        </div>
    )
}

export default NavUser