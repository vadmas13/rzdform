import * as React from 'react';
import {FC} from 'react'
import {StarUsualField, TitleField, TitleBorderText} from "../styled";
import {Popup} from 'semantic-ui-react'

const TitleFieldComp: FC<{ name: string, border?: boolean, requiredOff?: boolean }> = (props) => {
    const {name, border, requiredOff} = props;

    if (border) {
        return (
            <Popup
                trigger={<TitleField>
                    <TitleBorderText>{name}</TitleBorderText>
                    {!requiredOff && <StarUsualField>&#10059;</StarUsualField>}
                </TitleField>}
            >
                <Popup.Header style={{fontSize: '15px'}}>{ name }</Popup.Header>
                <Popup.Content  style={{fontSize: '13px'}}>
                    Подсказка для поля { name }
                </Popup.Content>
            </Popup>

        )
    } else {
        return (
            <TitleField>
                {name}
                {!requiredOff && <StarUsualField>&#10059;</StarUsualField>}
            </TitleField>


        )
    }

}

export default TitleFieldComp