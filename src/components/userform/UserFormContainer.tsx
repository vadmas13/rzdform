import * as React from 'react'
import {FC, useState} from "react";
import UserForm from "./UserForm";
import 'semantic-ui-css/semantic.min.css'
import {connect} from "react-redux";
import {InitialValues, initStateForm, PassengerType} from "../../redux/types/typesFormics";
import styled from './styledSemantic.module.scss'
import NavUser from "./NavUser/NavUser";
import {
    addNewPassenger,
    addPassenger,
    clearResponse,
    deletePassenger,
    editPassenger, responseSendForm
} from "../../redux/actions/actionForm";
import UserDataSuccess from "./UserDataSuccess/UserDataSuccess";
import ResponseFromServer from "./ResponseFromServer/ResponseFromServer";
import {AppStateType} from "../../redux/store";


type UserFormContainerProps = MapStatePropTypes & MapDispatchPropTypes & OwnPropTypes

type MapStatePropTypes = {
    form: initStateForm
}

type MapDispatchPropTypes = {
    editPassenger: (id: number) => void
    addNewPassenger: (id: number) => void
    deletePassenger: (id: number) => void
    clearResponse: () => void
    responseSendForm: (value: string) => void
    addPassenger: (passenger: InitialValues, id: number) => void
}

type OwnPropTypes = {

}

const UserFormContainer: FC<UserFormContainerProps> = (props): any => {
    const {form, addPassenger, editPassenger, clearResponse, responseSendForm,
        addNewPassenger, deletePassenger} = props;

    const [activePassenger, setActivePassenger] = useState<number>(1)


    if (form) {
        if (form.sendResponse) {
            return(
                <ResponseFromServer response={form.sendResponse} clearResponse={clearResponse}/>
            )
        } else {
            return (
                <div className={styled.containerForm}>
                    <div className={styled.containerForm__col}>
                        <NavUser activePassenger={activePassenger}
                                 setActivePassenger={setActivePassenger}
                                 addNewPassenger={addNewPassenger}
                                 responseSendForm={responseSendForm}
                                 form={form}
                        />
                    </div>
                    <div className={styled.containerForm__col}>
                        {
                            form.passengers.map((passenger: PassengerType, i: number) => {
                                if (passenger.id === activePassenger) {
                                    if (passenger.isEditMode) {
                                        return (
                                            <UserForm dataPassenger={passenger}
                                                      deletePassenger={deletePassenger}
                                                      addPassenger={addPassenger}
                                                      setActivePassenger={setActivePassenger}
                                                      canDelete={form.passengers.length > 1}
                                                      key={`${i}_passenger`}/>
                                        )
                                    } else {
                                        return <UserDataSuccess dataPassenger={passenger}
                                                                editPassenger={editPassenger}
                                        />
                                    }
                                } else return false
                            })}
                    </div>
                </div>
            )
        }
    } else return null

}

const mapStateToProps = (state: AppStateType): MapStatePropTypes => (
    {
        form: state.form
    }
)


export default connect<MapStatePropTypes, MapDispatchPropTypes, OwnPropTypes, AppStateType>(mapStateToProps,
    {addPassenger, editPassenger, responseSendForm,
        addNewPassenger, deletePassenger, clearResponse})(UserFormContainer)