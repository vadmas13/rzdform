import * as React from 'react';
import {FC} from 'react';
import {Form} from "semantic-ui-react";
import styledSemantic from "../styledSemantic.module.scss";
import TitleFieldComp from "../TitleField/TitleFieldComp";
import {ErrorMsg} from "../styled";
import {InitialValues} from "../../../redux/types/typesFormics";
import {docFields, itemDocFields} from '../../../config/docFields'


type propsType = {
    handleChange: (e: any) => any | void
    handleBlur: (fieldOrEvent: any) => any | void
    values: InitialValues
    touched: any
    errors: any
}

const DocumentFields: FC<propsType> = ({handleChange, handleBlur, values, touched, errors}): any => {

    const mapDocFields = !values.document || values.document === 'none' ? docFields.passport : docFields[values.document];

    return mapDocFields.map((item: itemDocFields, i: number) => {
        return (
            <Form.Field className={styledSemantic.form__Field} key={i}>
                <TitleFieldComp name={item.title} border={true}/>
                <input
                    type="text"
                    name={item.name}
                    style={touched[item.name] && errors[item.name] ? {border: '1px solid red'} : {}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values[item.name]}
                />
                <ErrorMsg>{errors[item.name] && touched[item.name] && errors[item.name]}</ErrorMsg>
            </Form.Field>
        )
    })


}

export default DocumentFields;