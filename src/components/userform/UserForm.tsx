import * as React from 'react'
import {FC} from "react";
import {Container, H1Title, TextField, ErrorMsg, HeaderForm, DeletePassenger} from './styled'
import {Form, Button} from 'semantic-ui-react'
import {Formik, Field} from 'formik';
import styledSemantic from './styledSemantic.module.scss'
import "react-datepicker/dist/react-datepicker.css";
import {registerLocale} from "react-datepicker";
import ru from 'date-fns/locale/ru';
import DatePickerField from "./DatePickerField/DatePickerField";
import TitleFieldComp from './TitleField/TitleFieldComp'

import {InitialValues, PassengerType} from '../../redux/types/typesFormics'
import validateForm from "../../config/validateForm";
import DocumentFields from "./DocumentFields/DocumentFields";

registerLocale('ru', ru)


//import {Button} from 'semantic-ui-react';
type UserFormPropsType = {
    dataPassenger: PassengerType,
    addPassenger?: (passenger: InitialValues, id: number) => void
    deletePassenger?: ( id: number) => void
    canDelete: boolean
    setActivePassenger: any
}

const UserForm: FC<UserFormPropsType> =
    ({dataPassenger, canDelete, setActivePassenger,
         addPassenger, deletePassenger}) => {

    const initialValues: InitialValues = dataPassenger.data;

    const deletePass = (id: number) => {
        if(deletePassenger){
            deletePassenger(id);
            setActivePassenger(1)
        }
    }

    return (
        <Container>
            <HeaderForm>
                <H1Title>
                    Пассажир №{dataPassenger.id}
                </H1Title>
                { canDelete && <DeletePassenger onClick={() => deletePass(dataPassenger.id)}>
                    Удалить пассажира
                </DeletePassenger> }
            </HeaderForm>
            <Formik
                initialValues={initialValues}
                validate={(values) => validateForm(values)}
                onSubmit={(values, {setSubmitting}) => {
                    if (addPassenger) {
                        addPassenger(values, dataPassenger.id)
                    }
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      isSubmitting
                  }) => (

                    <Form onSubmit={handleSubmit}>
                        <Form.Group>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'СНИЛС или номер регистрации ЦСМ'} requiredOff={true} border={true}/>
                                <input
                                    type="text"
                                    name="snils"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.snils}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal' className={styledSemantic.form__Group}>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'Фамилия'}/>
                                <input
                                    type="text"
                                    name="firstName"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    style={touched.firstName && errors.firstName ? {border: '1px solid red'} : {}}
                                    value={values.firstName}
                                />
                                <ErrorMsg>{errors.firstName && touched.firstName && errors.firstName}</ErrorMsg>
                            </Form.Field>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'Имя'}/>
                                <input
                                    type="text"
                                    name="secondName"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    style={touched.secondName && errors.secondName ? {border: '1px solid red'} : {}}
                                    value={values.secondName}
                                />
                                <ErrorMsg>{errors.secondName && touched.secondName && errors.secondName}</ErrorMsg>
                            </Form.Field>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp border={true} name={'Отчество (обязательно, при наличии)'}/>
                                <input
                                    type="text"
                                    name="thirdName"
                                    style={touched.thirdName && errors.thirdName ? {border: '1px solid red'} : {}}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.thirdName}
                                />
                                <ErrorMsg>{errors.thirdName && touched.thirdName && errors.thirdName}</ErrorMsg>
                            </Form.Field>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'Пол'}/>
                                <Field as="select" name="male" value={values.male ? values.male : 'none'}
                                       style={touched.male && errors.male ? {border: '1px solid red'} : {}}>
                                    <option value="none">Не выбрано</option>
                                    <option value="Мужской">Мужской</option>
                                    <option value="Женский">Женский</option>
                                </Field>
                                <ErrorMsg>{errors.male && touched.male && errors.male}</ErrorMsg>
                            </Form.Field>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'Дата рождения'}/>
                                <DatePickerField name="date"
                                                 style={touched.date && errors.date ? {border: '1px solid red'} : {}}/>
                                <ErrorMsg>{touched.date && errors.date && errors.date}</ErrorMsg>
                            </Form.Field>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'Гражданство'} border={true}/>
                                <Field as="select" name="nationality"
                                       value={values.nationality ? values.nationality : 'none'}
                                       style={touched.nationality && errors.nationality ? {border: '1px solid red'} : {}}>
                                    <option value="none">Не выбрано</option>
                                    <option value="Россия">Россия</option>
                                    <option value="Абхазия">Абхазия</option>
                                </Field>
                                <ErrorMsg>{errors.nationality && touched.nationality && errors.nationality}</ErrorMsg>
                            </Form.Field>
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'Тип документа'} border={true}/>
                                <Field as="select" name="document" value={values.document ? values.document : 'none'}
                                       style={touched.document && errors.document ? {border: '1px solid red'} : {}}>
                                    <option value="none">Не выбрано</option>
                                    <option value="passport">Паспорт</option>
                                    <option value="intPassport">Иностранный паспорт</option>
                                    <option value="intDocument">Иностранный документ</option>
                                    <option value="residence">Вид на жительство</option>
                                </Field>
                                <ErrorMsg>{errors.document && touched.document && errors.document}</ErrorMsg>
                            </Form.Field>
                            <DocumentFields handleBlur={handleBlur}
                                            handleChange={handleChange}
                                            values={values}
                                            touched={touched}
                                            errors={errors}
                            />
                            <Form.Field className={styledSemantic.form__Field}>
                                <TitleFieldComp name={'Тариф'}/>
                                <Field as="select" name="tariff" value={values.tariff ? values.tariff : 'none'}
                                       style={touched.tariff && errors.tariff ? {border: '1px solid red'} : {}}>
                                    <option value="none">Не выбрано</option>
                                    <option value="Полный">Полный</option>
                                    <option value="Не полный">Не полный</option>
                                </Field>
                                <ErrorMsg>{errors.tariff && touched.tariff && errors.tariff}</ErrorMsg>
                            </Form.Field>
                            <Form.Field className={styledSemantic.form__checkbox}>
                                <label className={styledSemantic.checkboxField}>
                                    <input type="checkbox"
                                           onChange={handleChange}
                                           onBlur={handleBlur}
                                           checked={values.alerts}
                                           name="alerts"/>
                                    <span className="">

                                    </span>
                                    Согласие на на получение оповещений в случаях чрезвычайных ситуаций
                                </label>
                            </Form.Field>
                            <Form.Field>
                                <TextField>
                                    Если вы готовы получать оповещения об изменении движения вашего поезда в случае
                                    чрезвычайной ситуации,
                                    укажите, пожалуйста, e-mail и/или телефон пассажира.
                                </TextField>
                                <TextField>
                                    Если не хотите получать оповещения - снимите галочку согласия на оповещения
                                </TextField>

                            </Form.Field>
                            {values.alerts && (
                                <Form.Field
                                    className={`${styledSemantic.form__Field} ${styledSemantic.form__Field_flex}`}>
                                    <TitleFieldComp name={'Телефон пассажира'} requiredOff={true}/>
                                    <input
                                        type="text"
                                        name="phone"
                                        onChange={handleChange}
                                        style={touched.phone && errors.phone ? {border: '1px solid red'} : {}}
                                        onBlur={handleBlur}
                                        value={values.phone}
                                    />
                                    <ErrorMsg>{errors.phone && touched.phone && errors.phone}</ErrorMsg>
                                </Form.Field>
                            )}
                            {values.alerts && (
                                <Form.Field
                                    className={`${styledSemantic.form__Field} ${styledSemantic.form__Field_flex}`}>
                                    <TitleFieldComp name={'E-mail пассажира'} requiredOff={true}/>
                                    <input
                                        type="text"
                                        name="email"
                                        style={touched.email && errors.email ? {border: '1px solid red'} : {}}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.email}
                                    />
                                    <ErrorMsg>{errors.email && touched.email && errors.email}</ErrorMsg>
                                </Form.Field>
                            )}
                            <div className={styledSemantic.form__errorField}>
                                {values.alerts && (
                                    <ErrorMsg>
                                        {errors.contants && (touched.email || touched.phone) && errors.contants}
                                    </ErrorMsg>
                                )}
                            </div>
                        </Form.Group>
                        <div className={styledSemantic.form__buttons}>
                            <Button content="Добавить пассажира"
                                    type="submit"
                                    inverted color='red'/>
                        </div>
                    </Form>
                )}
            </Formik>
        </Container>
    )
}


export default UserForm