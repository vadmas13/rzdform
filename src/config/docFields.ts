type docFieldType = {
    [key: string]: Array<itemDocFields>
}

export type itemDocFields = {
    name: string
    title: string
    requiredOff? :boolean
}

export const docFields: docFieldType = {
    passport: [{
        name: 'documentNumber',
        title: 'Номер документа'
    }],
    intPassport: [{
        name: 'documentNumber',
        title: 'Номер документа'
    }, {
        name: 'dateEndDoc',
        title: 'Окончание срока действия'
    }, {
        name: 'firstNameDoc',
        title: 'Фамилия (на латинице)'
    }, {
        name:'lastNameDoc',
        title: 'Имя на латинице'
    }, {
        name: 'thirdNameDoc',
        title: 'Отчество (на латинице)',
        requiredOff: true
    }],
    intDocument: [{
        name: 'internationalPerson',
        title: 'Гражданство'
    }, {
        name: 'documentNumber',
        title: 'Номер документа'
    }, {
        name: 'firstNameDoc',
        title: 'Фамилия (на латинице)'
    }, {
        name:'lastNameDoc',
        title: 'Имя на латинице'
    }, {
        name: 'thirdNameDoc',
        title: 'Отчество (на латинице)',
        requiredOff: true
    }],
    residence: [{
        name: 'documentNumber',
        title: 'Номер документа'
    }]
}