

export const sendDataToServer = (data: Array<{ [key: string]: any }>, responseSendForm: any) => {
    const jsonData = JSON.stringify(data);
    fetch('https://webhook.site/f63a87a7-413e-47f5-aca6-ff044ab25728', {
        method: 'POST',
        mode: 'cors',
        body: jsonData
    })
        .then(response => {
            const value = response.status + ' - Успешный статус ответа сервера. Успели на поезд!'
            responseSendForm(value)
        })
        .catch(error => {
            const value = error.message + ' - все очень плохо. Проверьте cors заголовки'
            responseSendForm(value)
        });
}