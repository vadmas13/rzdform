import {InitialValues} from "../redux/types/typesFormics";
import  moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru');

export const formFields: InitialValues = {
    firstName: '',
    secondName: '',
    thirdName: '',
    male: '',
    date: '',
    nationality: '',
    documentNumber: '',
    document: '',
    tariff: '',
    alerts: false,
    phone: '',
    email: '',
    dateEndDoc: '',
    firstNameDoc: '',
    lastNameDoc: '',
    thirdNameDoc: '',
    internationalPerson: '',
    snils: ''
}

export const formTitles: fieldsType = {
    firstName: 'Фамилия',
    secondName: 'Имя',
    thirdName: 'Отчество',
    male: 'Пол',
    date: 'Дата Рождения',
    nationality: 'Национальность',
    documentNumber: 'Номер документа',
    document: 'Тип документа',
    tariff: 'Тариф',
    alerts: 'Оповещения',
    phone: 'Телефон',
    email: 'E-mail',
    dateEndDoc: 'Окончание срока действия',
    firstNameDoc: 'Фамилия (на латинице)',
    lastNameDoc: 'Имя на латинице',
    thirdNameDoc: 'Отчество (на латинице)',
    internationalPerson: 'Гражданство',
    snils: 'СНИЛС или номер регистрации ЦСМ'
}

export const revertNames: {[key: string]: string} = {
    alerts: 'Да',
    passport: "Паспорт",
    intPassport: 'Иностранный паспорт',
    intDocument: 'Иностранный документ',
    residence: 'Вид на жительство'
}

export const revertValues = (object: {[key: string]: any}): {[key: string]: string}  => {
     let revertObject: {[key: string]: any} = {};
     Object.keys(object).forEach(item => {
        if(revertNames[object[item]]){
            revertObject[item] = revertNames[object[item]]
        }else if(item === 'alerts') {
            revertObject[item] = revertNames.alerts
        }else if(item === 'date'){
            revertObject[item] = moment(object[item]).subtract(10, 'days').calendar();
        }else{
            revertObject[item] = object[item].toString()
        }
    })

    return revertObject;
}

type fieldsType = {
    [key: string]: string
}