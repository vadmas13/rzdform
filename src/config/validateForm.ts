import {InitialValues} from "../redux/types/typesFormics";
import {docFields} from './docFields'

const validateEmail = (email: string) => {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
}

const validatePhone = (phone: string) => {
    // eslint-disable-next-line no-empty-character-class
    const re = /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/im;
    return re.test(phone);
}

const validateForm = (values: InitialValues & { [key: string]: any }) => {

    const errors: {
        [key: string]: string
    } = {};


    Object.keys(values).forEach((item: string) => {
        if (!values[item] && (item !== 'phone' && item !== 'email' && item !== 'alerts' && item !== 'snils')) {
            errors[item] = 'Поле обязательное'
        }

    })

    if (values.document && values.document !== 'none') {
        Object.keys(docFields).forEach(filed => {
            if (filed !== values.document) {
                docFields[filed].forEach(item => {
                    delete errors[item.name]
                })
            }
        })
        docFields[values.document].forEach(item => {
            if(!values[item.name]){
                errors[item.name] = 'Поле обязательное'
            }
        })
    }

    if (values.male === 'none') {
        errors.male = 'Выберите пол'
    }


    if (values.nationality === 'none') {
        errors.nationality = 'Выберите гражданство'
    }

    if (!values.date) {
        errors.date = 'Укажите дату рождения'
    }

    if (values.document === 'none') {
        errors.document = 'Выберите документ'
    }

    if (values.tariff === 'none') {
        errors.tariff = 'Выберите тариф'
    }


    if(values.alerts){

        if (values.email) {
            if (!validateEmail(values.email)) {
                errors.email = 'Некорректный E-mail'
            } else {
                delete errors.email
            }
        }

        if (values.phone) {
            if (!validatePhone(values.phone)) {
                errors.phone = 'Некорректный номер телефона'
            } else {
                delete errors.phone
            }
        }

        if(!values.phone && !values.email){
            errors.contants = 'Вы не указали контактные данные'
        }else{
            delete errors.contants
        }
    }


    return errors;
}

export default validateForm;